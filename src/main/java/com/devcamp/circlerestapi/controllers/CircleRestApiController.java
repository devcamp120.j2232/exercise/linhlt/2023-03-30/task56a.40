package com.devcamp.circlerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlerestapi.Circle;

@RestController
@RequestMapping
@CrossOrigin

public class CircleRestApiController{
    @GetMapping("circle-area")
    public double getArea(@RequestParam(required = true, name = "radius") double getArea){
        Circle circle1 = new Circle (1.0);
        System.out.println(circle1);
        return circle1.getArea();
    }
}
